/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kisanogroup.test.httpserver;

import fi.iki.elonen.NanoHTTPD;
import static fi.iki.elonen.NanoHTTPD.MIME_PLAINTEXT;
import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ixm
 */
public class HttpServer extends NanoHTTPD {
    private final Logger logger = LogManager.getLogger(HttpServer.class);

    private static final Integer[] portRange = {
        2375, 2376, 2377, 2378, 2379, 2380
    };

    public enum RequestType {
        DISCOVERY, UUID_GENERATOR, UNKNOWN
    }

    private static HttpServer server = null;

    public static HttpServer getInstance() {
        return server;
    }

    protected HttpServer(int port) throws IOException {
        super(port);
    }

    @Override
    public Response serve(String uri, Method method, Map<String, String> header, Map<String, String> params, Map<String, String> files) {
        RequestType requestType;
        try {
            requestType = getRequestType(uri);
        } catch (Exception ex) {
            return setHeaders(newFixedLengthResponse(Response.Status.BAD_REQUEST, MIME_PLAINTEXT, ex.getMessage()));
        }

        switch(requestType) {
            case DISCOVERY: {
                return setHeaders(discovery(params));
            }

            case UUID_GENERATOR: {
                return setHeaders(uuidGenerator());
            }

            default: {
                return setHeaders(newFixedLengthResponse(Response.Status.NOT_FOUND, MIME_PLAINTEXT, "Unknown request type"));
            }
        }
    }

    protected Response discovery(Map<String, String> params) {
        if(params == null || !params.containsKey("name")) {
            logger.error("No \"name\" entry in params");
            return newFixedLengthResponse(Response.Status.BAD_REQUEST, MIME_PLAINTEXT, "No \"name\" entry in params");
        }

        return newFixedLengthResponse(Response.Status.OK, MIME_PLAINTEXT, "Hi "+ params.get("name") +", I'm KISANO server");
    }

    protected Response uuidGenerator() {
        return newFixedLengthResponse(Response.Status.OK, MIME_PLAINTEXT, UUID.randomUUID().toString());
    }

    protected RequestType getRequestType(String uri) throws Exception {
        URI uriObj;
        try {
            uriObj = new URI(uri);
        } catch (NullPointerException|URISyntaxException ex) {
            logger.error("Could not parse URI: "+ ex.getMessage(), ex);
            throw new Exception("Could not parse URI: "+ ex.getMessage(), ex);
        }

        String path = uriObj.getPath();

        switch(StringUtils.defaultString(path)) {
            case "/discovery":
                return RequestType.DISCOVERY;

            case "/uuid":
                return RequestType.UUID_GENERATOR;

            default:
                return RequestType.UNKNOWN;
        }
    }

    protected Response setHeaders(Response resp) {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Max-Age", "3628800");
        resp.addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
        resp.addHeader("Access-Control-Allow-Headers", "Authorization");
        return resp;
    }

    public static boolean startServer() {
        int i = 0;
        List<Integer> ports = new ArrayList<>(Arrays.asList(HttpServer.portRange));

        Collections.shuffle(ports);

        while (server == null && i < ports.size()) {
            int tmpPort = ports.get(i++);

            try {
                server = new HttpServer(tmpPort);
                server.start();
                LogManager.getRootLogger().info("HTTP server started on port: "+ tmpPort);
                return true;
            } catch (IOException ex) {
                LogManager.getRootLogger().info("Cannot listen on port: "+ tmpPort + "."+ (i + 1 >= portRange.length ? " All ports have been tested" : "Trying port: "+ portRange[i+1]), ex);
                if(server != null && server.wasStarted())
                    server.stop();
                server = null;
            }
        }

        return false;
    }
}
